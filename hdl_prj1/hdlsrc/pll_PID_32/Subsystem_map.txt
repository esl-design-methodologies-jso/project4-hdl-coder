pll_PID_32/Subsystem/Discrete PID Controller --> Discrete_PID_Controller
pll_PID_32/Subsystem/VCO /Cosine HDL Optimized --> Cosine_HDL_Optimized
pll_PID_32/Subsystem/VCO  --> VCO
pll_PID_32/Subsystem/paseComparator /nfp_convert_sfix_16_En14_to_single --> nfp_convert_sfix_16_En14_to_single
pll_PID_32/Subsystem/paseComparator /nfp_convert_single_to_sfix_32_En29 --> nfp_convert_single_to_sfix_32_En29
pll_PID_32/Subsystem/paseComparator /nfp_atan2_comp --> nfp_atan2_comp
pll_PID_32/Subsystem/paseComparator  --> paseComparator
pll_PID_32/Subsystem --> Subsystem
