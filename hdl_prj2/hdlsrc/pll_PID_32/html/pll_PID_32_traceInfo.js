function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <S1>/Delay */
	this.urlHashMap["pll_PID_32:23"] = "Subsystem.v:82";
	/* <S1>/Discrete PID Controller */
	this.urlHashMap["pll_PID_32:20"] = "Subsystem.v:74";
	/* <S2>/Derivative Gain */
	this.urlHashMap["pll_PID_32:20:1690"] = "Discrete_PID_Controller.v:111";
	/* <S2>/Filter */
	this.urlHashMap["pll_PID_32:20:1692"] = "Discrete_PID_Controller.v:134";
	/* <S2>/Filter Coefficient */
	this.urlHashMap["pll_PID_32:20:1693"] = "Discrete_PID_Controller.v:157";
	/* <S2>/Integral Gain */
	this.urlHashMap["pll_PID_32:20:1689"] = "Discrete_PID_Controller.v:73";
	/* <S2>/Integrator */
	this.urlHashMap["pll_PID_32:20:1691"] = "Discrete_PID_Controller.v:96";
	/* <S2>/Proportional Gain */
	this.urlHashMap["pll_PID_32:20:1688"] = "Discrete_PID_Controller.v:67";
	/* <S2>/Sum */
	this.urlHashMap["pll_PID_32:20:1687"] = "Discrete_PID_Controller.v:163";
	/* <S2>/SumD */
	this.urlHashMap["pll_PID_32:20:1694"] = "Discrete_PID_Controller.v:149";
	/* <S3>/Cosine
HDL Optimized */
	this.urlHashMap["pll_PID_32:14"] = "VCO.v:71";
	/* <S3>/Data Type Conversion */
	this.urlHashMap["pll_PID_32:42"] = "VCO.v:77";
	/* <S3>/Delay */
	this.urlHashMap["pll_PID_32:16"] = "VCO.v:48";
	/* <S3>/adder
 */
	this.urlHashMap["pll_PID_32:15"] = "msg=rtwMsg_notTraceable&block=pll_PID_32:15";
	/* <S4>/Complex to
Real-Imag */
	this.urlHashMap["pll_PID_32:1"] = "paseComparator.v:84";
	/* <S4>/Data Type Conversion */
	this.urlHashMap["pll_PID_32:48"] = "paseComparator.v:73";
	/* <S4>/Data Type Conversion1 */
	this.urlHashMap["pll_PID_32:49"] = "paseComparator.v:89";
	/* <S4>/Data Type Conversion2 */
	this.urlHashMap["pll_PID_32:50"] = "paseComparator.v:100";
	/* <S4>/Product */
	this.urlHashMap["pll_PID_32:2"] = "paseComparator.v:51,57,63,78";
	/* <S4>/Trigonometric
Function */
	this.urlHashMap["pll_PID_32:47"] = "paseComparator.v:94";
	/* <S5>/exp hdl */
	this.urlHashMap["pll_PID_32:14:192"] = "Cosine_HDL_Optimized.v:121";
	/* <S6>/Real-Imag to
Complex */
	this.urlHashMap["pll_PID_32:14:192:7"] = "Cosine_HDL_Optimized.v:349";
	/* <S6>/cos_hdl */
	this.urlHashMap["pll_PID_32:14:192:35"] = "Cosine_HDL_Optimized.v:119";
	/* <S6>/sin_hdl */
	this.urlHashMap["pll_PID_32:14:192:36"] = "Cosine_HDL_Optimized.v:396";
	/* <S7>/1st or 4th Quad */
	this.urlHashMap["pll_PID_32:14:192:35:7"] = "Cosine_HDL_Optimized.v:143";
	/* <S7>/Amp25 */
	this.urlHashMap["pll_PID_32:14:192:35:8"] = "Cosine_HDL_Optimized.v:177";
	/* <S7>/Amp75 */
	this.urlHashMap["pll_PID_32:14:192:35:9"] = "Cosine_HDL_Optimized.v:170";
	/* <S7>/CastU16En1 */
	this.urlHashMap["pll_PID_32:14:192:35:11"] = "Cosine_HDL_Optimized.v:227";
	/* <S7>/CastU16En2 */
	this.urlHashMap["pll_PID_32:14:192:35:13"] = "Cosine_HDL_Optimized.v:245";
	/* <S7>/CastU16En3 */
	this.urlHashMap["pll_PID_32:14:192:35:14"] = "Cosine_HDL_Optimized.v:232";
	/* <S7>/GTEp75 */
	this.urlHashMap["pll_PID_32:14:192:35:18"] = "Cosine_HDL_Optimized.v:137";
	/* <S7>/LTEp25 */
	this.urlHashMap["pll_PID_32:14:192:35:20"] = "Cosine_HDL_Optimized.v:126";
	/* <S7>/LTEp50 */
	this.urlHashMap["pll_PID_32:14:192:35:21"] = "Cosine_HDL_Optimized.v:191";
	/* <S7>/Look-Up
Table */
	this.urlHashMap["pll_PID_32:14:192:35:22"] = "Cosine_HDL_Optimized.v:262";
	/* <S7>/Negate */
	this.urlHashMap["pll_PID_32:14:192:35:23"] = "Cosine_HDL_Optimized.v:336";
	/* <S7>/Point25 */
	this.urlHashMap["pll_PID_32:14:192:35:24"] = "Cosine_HDL_Optimized.v:117";
	/* <S7>/Point50 */
	this.urlHashMap["pll_PID_32:14:192:35:25"] = "Cosine_HDL_Optimized.v:158";
	/* <S7>/Point75 */
	this.urlHashMap["pll_PID_32:14:192:35:26"] = "Cosine_HDL_Optimized.v:132";
	/* <S7>/Positive */
	this.urlHashMap["pll_PID_32:14:192:35:83"] = "Cosine_HDL_Optimized.v:264";
	/* <S7>/QuadHandle1a */
	this.urlHashMap["pll_PID_32:14:192:35:27"] = "Cosine_HDL_Optimized.v:209";
	/* <S7>/QuadHandle1b */
	this.urlHashMap["pll_PID_32:14:192:35:28"] = "Cosine_HDL_Optimized.v:197";
	/* <S7>/QuadHandle2 */
	this.urlHashMap["pll_PID_32:14:192:35:29"] = "Cosine_HDL_Optimized.v:221";
	/* <S7>/RAMDelayBalance */
	this.urlHashMap["pll_PID_32:14:192:35:205"] = "Cosine_HDL_Optimized.v:148";
	/* <S7>/Saturation */
	this.urlHashMap["pll_PID_32:14:192:35:30"] = "Cosine_HDL_Optimized.v:250";
	/* <S7>/SignCorrected */
	this.urlHashMap["pll_PID_32:14:192:35:31"] = "Cosine_HDL_Optimized.v:343";
	/* <S7>/Switch */
	this.urlHashMap["pll_PID_32:14:192:35:79"] = "Cosine_HDL_Optimized.v:256";
	/* <S7>/insig */
	this.urlHashMap["pll_PID_32:14:192:35:12"] = "Cosine_HDL_Optimized.v:112";
	/* <S7>/p25mA */
	this.urlHashMap["pll_PID_32:14:192:35:33"] = "Cosine_HDL_Optimized.v:184";
	/* <S7>/p75mA */
	this.urlHashMap["pll_PID_32:14:192:35:34"] = "Cosine_HDL_Optimized.v:163";
	/* <S7>/pow2switch */
	this.urlHashMap["pll_PID_32:14:192:35:80"] = "Cosine_HDL_Optimized.v:153";
	/* <S7>/x4 */
	this.urlHashMap["pll_PID_32:14:192:35:10"] = "Cosine_HDL_Optimized.v:240";
	/* <S8>/Amp50 */
	this.urlHashMap["pll_PID_32:14:192:36:44"] = "Cosine_HDL_Optimized.v:377";
	/* <S8>/CastU16En2 */
	this.urlHashMap["pll_PID_32:14:192:36:62"] = "Cosine_HDL_Optimized.v:428";
	/* <S8>/CastU16En3 */
	this.urlHashMap["pll_PID_32:14:192:36:63"] = "Cosine_HDL_Optimized.v:446";
	/* <S8>/CastU16En4 */
	this.urlHashMap["pll_PID_32:14:192:36:64"] = "Cosine_HDL_Optimized.v:433";
	/* <S8>/LTEp25 */
	this.urlHashMap["pll_PID_32:14:192:36:47"] = "Cosine_HDL_Optimized.v:401";
	/* <S8>/LTEp50 */
	this.urlHashMap["pll_PID_32:14:192:36:48"] = "Cosine_HDL_Optimized.v:364";
	/* <S8>/Look-Up
Table */
	this.urlHashMap["pll_PID_32:14:192:36:65"] = "Cosine_HDL_Optimized.v:463";
	/* <S8>/Negate */
	this.urlHashMap["pll_PID_32:14:192:36:50"] = "Cosine_HDL_Optimized.v:537";
	/* <S8>/Point25 */
	this.urlHashMap["pll_PID_32:14:192:36:51"] = "Cosine_HDL_Optimized.v:394";
	/* <S8>/Point50 */
	this.urlHashMap["pll_PID_32:14:192:36:52"] = "Cosine_HDL_Optimized.v:359";
	/* <S8>/Positive */
	this.urlHashMap["pll_PID_32:14:192:36:84"] = "Cosine_HDL_Optimized.v:465";
	/* <S8>/QuadHandle1 */
	this.urlHashMap["pll_PID_32:14:192:36:53"] = "Cosine_HDL_Optimized.v:388";
	/* <S8>/QuadHandle2 */
	this.urlHashMap["pll_PID_32:14:192:36:54"] = "Cosine_HDL_Optimized.v:422";
	/* <S8>/RAMDelayBalance */
	this.urlHashMap["pll_PID_32:14:192:36:177"] = "Cosine_HDL_Optimized.v:366";
	/* <S8>/Saturation */
	this.urlHashMap["pll_PID_32:14:192:36:66"] = "Cosine_HDL_Optimized.v:451";
	/* <S8>/SignCorrected */
	this.urlHashMap["pll_PID_32:14:192:36:55"] = "Cosine_HDL_Optimized.v:544";
	/* <S8>/Switch */
	this.urlHashMap["pll_PID_32:14:192:36:79"] = "Cosine_HDL_Optimized.v:457";
	/* <S8>/insig */
	this.urlHashMap["pll_PID_32:14:192:36:46"] = "Cosine_HDL_Optimized.v:354";
	/* <S8>/p50mA */
	this.urlHashMap["pll_PID_32:14:192:36:57"] = "Cosine_HDL_Optimized.v:407";
	/* <S8>/pow2switch */
	this.urlHashMap["pll_PID_32:14:192:36:80"] = "Cosine_HDL_Optimized.v:372";
	/* <S8>/x4 */
	this.urlHashMap["pll_PID_32:14:192:36:68"] = "Cosine_HDL_Optimized.v:441";
	/* <S9>/bit_shift */
	this.urlHashMap["pll_PID_32:14:192:35:10:481"] = "msg=rtwMsg_notTraceable&block=pll_PID_32:14:192:35:10:481";
	/* <S10>:1 */
	this.urlHashMap["pll_PID_32:14:192:35:10:481:1"] = "msg=rtwMsg_optimizedSfObject&block=pll_PID_32:14:192:35:10:481:1";
	/* <S11>/bit_shift */
	this.urlHashMap["pll_PID_32:14:192:36:68:481"] = "msg=rtwMsg_notTraceable&block=pll_PID_32:14:192:36:68:481";
	/* <S12>:1 */
	this.urlHashMap["pll_PID_32:14:192:36:68:481:1"] = "msg=rtwMsg_optimizedSfObject&block=pll_PID_32:14:192:36:68:481:1";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "pll_PID_32"};
	this.sidHashMap["pll_PID_32"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S1>/In1"] = {sid: "pll_PID_32:34"};
	this.sidHashMap["pll_PID_32:34"] = {rtwname: "<S1>/In1"};
	this.rtwnameHashMap["<S1>/Delay"] = {sid: "pll_PID_32:23"};
	this.sidHashMap["pll_PID_32:23"] = {rtwname: "<S1>/Delay"};
	this.rtwnameHashMap["<S1>/Discrete PID Controller"] = {sid: "pll_PID_32:20"};
	this.sidHashMap["pll_PID_32:20"] = {rtwname: "<S1>/Discrete PID Controller"};
	this.rtwnameHashMap["<S1>/VCO "] = {sid: "pll_PID_32:17"};
	this.sidHashMap["pll_PID_32:17"] = {rtwname: "<S1>/VCO "};
	this.rtwnameHashMap["<S1>/paseComparator "] = {sid: "pll_PID_32:7"};
	this.sidHashMap["pll_PID_32:7"] = {rtwname: "<S1>/paseComparator "};
	this.rtwnameHashMap["<S1>/Out1"] = {sid: "pll_PID_32:35"};
	this.sidHashMap["pll_PID_32:35"] = {rtwname: "<S1>/Out1"};
	this.rtwnameHashMap["<S1>/Out2"] = {sid: "pll_PID_32:36"};
	this.sidHashMap["pll_PID_32:36"] = {rtwname: "<S1>/Out2"};
	this.rtwnameHashMap["<S2>/u"] = {sid: "pll_PID_32:20:1"};
	this.sidHashMap["pll_PID_32:20:1"] = {rtwname: "<S2>/u"};
	this.rtwnameHashMap["<S2>/Derivative Gain"] = {sid: "pll_PID_32:20:1690"};
	this.sidHashMap["pll_PID_32:20:1690"] = {rtwname: "<S2>/Derivative Gain"};
	this.rtwnameHashMap["<S2>/Filter"] = {sid: "pll_PID_32:20:1692"};
	this.sidHashMap["pll_PID_32:20:1692"] = {rtwname: "<S2>/Filter"};
	this.rtwnameHashMap["<S2>/Filter Coefficient"] = {sid: "pll_PID_32:20:1693"};
	this.sidHashMap["pll_PID_32:20:1693"] = {rtwname: "<S2>/Filter Coefficient"};
	this.rtwnameHashMap["<S2>/Integral Gain"] = {sid: "pll_PID_32:20:1689"};
	this.sidHashMap["pll_PID_32:20:1689"] = {rtwname: "<S2>/Integral Gain"};
	this.rtwnameHashMap["<S2>/Integrator"] = {sid: "pll_PID_32:20:1691"};
	this.sidHashMap["pll_PID_32:20:1691"] = {rtwname: "<S2>/Integrator"};
	this.rtwnameHashMap["<S2>/Proportional Gain"] = {sid: "pll_PID_32:20:1688"};
	this.sidHashMap["pll_PID_32:20:1688"] = {rtwname: "<S2>/Proportional Gain"};
	this.rtwnameHashMap["<S2>/Sum"] = {sid: "pll_PID_32:20:1687"};
	this.sidHashMap["pll_PID_32:20:1687"] = {rtwname: "<S2>/Sum"};
	this.rtwnameHashMap["<S2>/SumD"] = {sid: "pll_PID_32:20:1694"};
	this.sidHashMap["pll_PID_32:20:1694"] = {rtwname: "<S2>/SumD"};
	this.rtwnameHashMap["<S2>/y"] = {sid: "pll_PID_32:20:10"};
	this.sidHashMap["pll_PID_32:20:10"] = {rtwname: "<S2>/y"};
	this.rtwnameHashMap["<S3>/In1"] = {sid: "pll_PID_32:18"};
	this.sidHashMap["pll_PID_32:18"] = {rtwname: "<S3>/In1"};
	this.rtwnameHashMap["<S3>/Cosine HDL Optimized"] = {sid: "pll_PID_32:14"};
	this.sidHashMap["pll_PID_32:14"] = {rtwname: "<S3>/Cosine HDL Optimized"};
	this.rtwnameHashMap["<S3>/Data Type Conversion"] = {sid: "pll_PID_32:42"};
	this.sidHashMap["pll_PID_32:42"] = {rtwname: "<S3>/Data Type Conversion"};
	this.rtwnameHashMap["<S3>/Delay"] = {sid: "pll_PID_32:16"};
	this.sidHashMap["pll_PID_32:16"] = {rtwname: "<S3>/Delay"};
	this.rtwnameHashMap["<S3>/adder "] = {sid: "pll_PID_32:15"};
	this.sidHashMap["pll_PID_32:15"] = {rtwname: "<S3>/adder "};
	this.rtwnameHashMap["<S3>/Out1"] = {sid: "pll_PID_32:19"};
	this.sidHashMap["pll_PID_32:19"] = {rtwname: "<S3>/Out1"};
	this.rtwnameHashMap["<S4>/In1"] = {sid: "pll_PID_32:11"};
	this.sidHashMap["pll_PID_32:11"] = {rtwname: "<S4>/In1"};
	this.rtwnameHashMap["<S4>/In2"] = {sid: "pll_PID_32:12"};
	this.sidHashMap["pll_PID_32:12"] = {rtwname: "<S4>/In2"};
	this.rtwnameHashMap["<S4>/Complex to Real-Imag"] = {sid: "pll_PID_32:1"};
	this.sidHashMap["pll_PID_32:1"] = {rtwname: "<S4>/Complex to Real-Imag"};
	this.rtwnameHashMap["<S4>/Data Type Conversion"] = {sid: "pll_PID_32:48"};
	this.sidHashMap["pll_PID_32:48"] = {rtwname: "<S4>/Data Type Conversion"};
	this.rtwnameHashMap["<S4>/Data Type Conversion1"] = {sid: "pll_PID_32:49"};
	this.sidHashMap["pll_PID_32:49"] = {rtwname: "<S4>/Data Type Conversion1"};
	this.rtwnameHashMap["<S4>/Data Type Conversion2"] = {sid: "pll_PID_32:50"};
	this.sidHashMap["pll_PID_32:50"] = {rtwname: "<S4>/Data Type Conversion2"};
	this.rtwnameHashMap["<S4>/Product"] = {sid: "pll_PID_32:2"};
	this.sidHashMap["pll_PID_32:2"] = {rtwname: "<S4>/Product"};
	this.rtwnameHashMap["<S4>/Trigonometric Function"] = {sid: "pll_PID_32:47"};
	this.sidHashMap["pll_PID_32:47"] = {rtwname: "<S4>/Trigonometric Function"};
	this.rtwnameHashMap["<S4>/Out1"] = {sid: "pll_PID_32:13"};
	this.sidHashMap["pll_PID_32:13"] = {rtwname: "<S4>/Out1"};
	this.rtwnameHashMap["<S5>/u"] = {sid: "pll_PID_32:14:6"};
	this.sidHashMap["pll_PID_32:14:6"] = {rtwname: "<S5>/u"};
	this.rtwnameHashMap["<S5>/exp hdl"] = {sid: "pll_PID_32:14:192"};
	this.sidHashMap["pll_PID_32:14:192"] = {rtwname: "<S5>/exp hdl"};
	this.rtwnameHashMap["<S5>/x"] = {sid: "pll_PID_32:14:36"};
	this.sidHashMap["pll_PID_32:14:36"] = {rtwname: "<S5>/x"};
	this.rtwnameHashMap["<S6>/In1"] = {sid: "pll_PID_32:14:192:6"};
	this.sidHashMap["pll_PID_32:14:192:6"] = {rtwname: "<S6>/In1"};
	this.rtwnameHashMap["<S6>/Real-Imag to Complex"] = {sid: "pll_PID_32:14:192:7"};
	this.sidHashMap["pll_PID_32:14:192:7"] = {rtwname: "<S6>/Real-Imag to Complex"};
	this.rtwnameHashMap["<S6>/cos_hdl"] = {sid: "pll_PID_32:14:192:35"};
	this.sidHashMap["pll_PID_32:14:192:35"] = {rtwname: "<S6>/cos_hdl"};
	this.rtwnameHashMap["<S6>/sin_hdl"] = {sid: "pll_PID_32:14:192:36"};
	this.sidHashMap["pll_PID_32:14:192:36"] = {rtwname: "<S6>/sin_hdl"};
	this.rtwnameHashMap["<S6>/Out1"] = {sid: "pll_PID_32:14:192:10"};
	this.sidHashMap["pll_PID_32:14:192:10"] = {rtwname: "<S6>/Out1"};
	this.rtwnameHashMap["<S7>/In1"] = {sid: "pll_PID_32:14:192:35:6"};
	this.sidHashMap["pll_PID_32:14:192:35:6"] = {rtwname: "<S7>/In1"};
	this.rtwnameHashMap["<S7>/1st or 4th Quad"] = {sid: "pll_PID_32:14:192:35:7"};
	this.sidHashMap["pll_PID_32:14:192:35:7"] = {rtwname: "<S7>/1st or 4th Quad"};
	this.rtwnameHashMap["<S7>/Amp25"] = {sid: "pll_PID_32:14:192:35:8"};
	this.sidHashMap["pll_PID_32:14:192:35:8"] = {rtwname: "<S7>/Amp25"};
	this.rtwnameHashMap["<S7>/Amp75"] = {sid: "pll_PID_32:14:192:35:9"};
	this.sidHashMap["pll_PID_32:14:192:35:9"] = {rtwname: "<S7>/Amp75"};
	this.rtwnameHashMap["<S7>/CastU16En1"] = {sid: "pll_PID_32:14:192:35:11"};
	this.sidHashMap["pll_PID_32:14:192:35:11"] = {rtwname: "<S7>/CastU16En1"};
	this.rtwnameHashMap["<S7>/CastU16En2"] = {sid: "pll_PID_32:14:192:35:13"};
	this.sidHashMap["pll_PID_32:14:192:35:13"] = {rtwname: "<S7>/CastU16En2"};
	this.rtwnameHashMap["<S7>/CastU16En3"] = {sid: "pll_PID_32:14:192:35:14"};
	this.sidHashMap["pll_PID_32:14:192:35:14"] = {rtwname: "<S7>/CastU16En3"};
	this.rtwnameHashMap["<S7>/GTEp75"] = {sid: "pll_PID_32:14:192:35:18"};
	this.sidHashMap["pll_PID_32:14:192:35:18"] = {rtwname: "<S7>/GTEp75"};
	this.rtwnameHashMap["<S7>/LTEp25"] = {sid: "pll_PID_32:14:192:35:20"};
	this.sidHashMap["pll_PID_32:14:192:35:20"] = {rtwname: "<S7>/LTEp25"};
	this.rtwnameHashMap["<S7>/LTEp50"] = {sid: "pll_PID_32:14:192:35:21"};
	this.sidHashMap["pll_PID_32:14:192:35:21"] = {rtwname: "<S7>/LTEp50"};
	this.rtwnameHashMap["<S7>/Look-Up Table"] = {sid: "pll_PID_32:14:192:35:22"};
	this.sidHashMap["pll_PID_32:14:192:35:22"] = {rtwname: "<S7>/Look-Up Table"};
	this.rtwnameHashMap["<S7>/Negate"] = {sid: "pll_PID_32:14:192:35:23"};
	this.sidHashMap["pll_PID_32:14:192:35:23"] = {rtwname: "<S7>/Negate"};
	this.rtwnameHashMap["<S7>/Point25"] = {sid: "pll_PID_32:14:192:35:24"};
	this.sidHashMap["pll_PID_32:14:192:35:24"] = {rtwname: "<S7>/Point25"};
	this.rtwnameHashMap["<S7>/Point50"] = {sid: "pll_PID_32:14:192:35:25"};
	this.sidHashMap["pll_PID_32:14:192:35:25"] = {rtwname: "<S7>/Point50"};
	this.rtwnameHashMap["<S7>/Point75"] = {sid: "pll_PID_32:14:192:35:26"};
	this.sidHashMap["pll_PID_32:14:192:35:26"] = {rtwname: "<S7>/Point75"};
	this.rtwnameHashMap["<S7>/Positive"] = {sid: "pll_PID_32:14:192:35:83"};
	this.sidHashMap["pll_PID_32:14:192:35:83"] = {rtwname: "<S7>/Positive"};
	this.rtwnameHashMap["<S7>/QuadHandle1a"] = {sid: "pll_PID_32:14:192:35:27"};
	this.sidHashMap["pll_PID_32:14:192:35:27"] = {rtwname: "<S7>/QuadHandle1a"};
	this.rtwnameHashMap["<S7>/QuadHandle1b"] = {sid: "pll_PID_32:14:192:35:28"};
	this.sidHashMap["pll_PID_32:14:192:35:28"] = {rtwname: "<S7>/QuadHandle1b"};
	this.rtwnameHashMap["<S7>/QuadHandle2"] = {sid: "pll_PID_32:14:192:35:29"};
	this.sidHashMap["pll_PID_32:14:192:35:29"] = {rtwname: "<S7>/QuadHandle2"};
	this.rtwnameHashMap["<S7>/RAMDelayBalance"] = {sid: "pll_PID_32:14:192:35:205"};
	this.sidHashMap["pll_PID_32:14:192:35:205"] = {rtwname: "<S7>/RAMDelayBalance"};
	this.rtwnameHashMap["<S7>/Saturation"] = {sid: "pll_PID_32:14:192:35:30"};
	this.sidHashMap["pll_PID_32:14:192:35:30"] = {rtwname: "<S7>/Saturation"};
	this.rtwnameHashMap["<S7>/SignCorrected"] = {sid: "pll_PID_32:14:192:35:31"};
	this.sidHashMap["pll_PID_32:14:192:35:31"] = {rtwname: "<S7>/SignCorrected"};
	this.rtwnameHashMap["<S7>/Switch"] = {sid: "pll_PID_32:14:192:35:79"};
	this.sidHashMap["pll_PID_32:14:192:35:79"] = {rtwname: "<S7>/Switch"};
	this.rtwnameHashMap["<S7>/insig"] = {sid: "pll_PID_32:14:192:35:12"};
	this.sidHashMap["pll_PID_32:14:192:35:12"] = {rtwname: "<S7>/insig"};
	this.rtwnameHashMap["<S7>/p25mA"] = {sid: "pll_PID_32:14:192:35:33"};
	this.sidHashMap["pll_PID_32:14:192:35:33"] = {rtwname: "<S7>/p25mA"};
	this.rtwnameHashMap["<S7>/p75mA"] = {sid: "pll_PID_32:14:192:35:34"};
	this.sidHashMap["pll_PID_32:14:192:35:34"] = {rtwname: "<S7>/p75mA"};
	this.rtwnameHashMap["<S7>/pow2switch"] = {sid: "pll_PID_32:14:192:35:80"};
	this.sidHashMap["pll_PID_32:14:192:35:80"] = {rtwname: "<S7>/pow2switch"};
	this.rtwnameHashMap["<S7>/x4"] = {sid: "pll_PID_32:14:192:35:10"};
	this.sidHashMap["pll_PID_32:14:192:35:10"] = {rtwname: "<S7>/x4"};
	this.rtwnameHashMap["<S7>/x"] = {sid: "pll_PID_32:14:192:35:36"};
	this.sidHashMap["pll_PID_32:14:192:35:36"] = {rtwname: "<S7>/x"};
	this.rtwnameHashMap["<S8>/In1"] = {sid: "pll_PID_32:14:192:36:60"};
	this.sidHashMap["pll_PID_32:14:192:36:60"] = {rtwname: "<S8>/In1"};
	this.rtwnameHashMap["<S8>/Amp50"] = {sid: "pll_PID_32:14:192:36:44"};
	this.sidHashMap["pll_PID_32:14:192:36:44"] = {rtwname: "<S8>/Amp50"};
	this.rtwnameHashMap["<S8>/CastU16En2"] = {sid: "pll_PID_32:14:192:36:62"};
	this.sidHashMap["pll_PID_32:14:192:36:62"] = {rtwname: "<S8>/CastU16En2"};
	this.rtwnameHashMap["<S8>/CastU16En3"] = {sid: "pll_PID_32:14:192:36:63"};
	this.sidHashMap["pll_PID_32:14:192:36:63"] = {rtwname: "<S8>/CastU16En3"};
	this.rtwnameHashMap["<S8>/CastU16En4"] = {sid: "pll_PID_32:14:192:36:64"};
	this.sidHashMap["pll_PID_32:14:192:36:64"] = {rtwname: "<S8>/CastU16En4"};
	this.rtwnameHashMap["<S8>/LTEp25"] = {sid: "pll_PID_32:14:192:36:47"};
	this.sidHashMap["pll_PID_32:14:192:36:47"] = {rtwname: "<S8>/LTEp25"};
	this.rtwnameHashMap["<S8>/LTEp50"] = {sid: "pll_PID_32:14:192:36:48"};
	this.sidHashMap["pll_PID_32:14:192:36:48"] = {rtwname: "<S8>/LTEp50"};
	this.rtwnameHashMap["<S8>/Look-Up Table"] = {sid: "pll_PID_32:14:192:36:65"};
	this.sidHashMap["pll_PID_32:14:192:36:65"] = {rtwname: "<S8>/Look-Up Table"};
	this.rtwnameHashMap["<S8>/Negate"] = {sid: "pll_PID_32:14:192:36:50"};
	this.sidHashMap["pll_PID_32:14:192:36:50"] = {rtwname: "<S8>/Negate"};
	this.rtwnameHashMap["<S8>/Point25"] = {sid: "pll_PID_32:14:192:36:51"};
	this.sidHashMap["pll_PID_32:14:192:36:51"] = {rtwname: "<S8>/Point25"};
	this.rtwnameHashMap["<S8>/Point50"] = {sid: "pll_PID_32:14:192:36:52"};
	this.sidHashMap["pll_PID_32:14:192:36:52"] = {rtwname: "<S8>/Point50"};
	this.rtwnameHashMap["<S8>/Positive"] = {sid: "pll_PID_32:14:192:36:84"};
	this.sidHashMap["pll_PID_32:14:192:36:84"] = {rtwname: "<S8>/Positive"};
	this.rtwnameHashMap["<S8>/QuadHandle1"] = {sid: "pll_PID_32:14:192:36:53"};
	this.sidHashMap["pll_PID_32:14:192:36:53"] = {rtwname: "<S8>/QuadHandle1"};
	this.rtwnameHashMap["<S8>/QuadHandle2"] = {sid: "pll_PID_32:14:192:36:54"};
	this.sidHashMap["pll_PID_32:14:192:36:54"] = {rtwname: "<S8>/QuadHandle2"};
	this.rtwnameHashMap["<S8>/RAMDelayBalance"] = {sid: "pll_PID_32:14:192:36:177"};
	this.sidHashMap["pll_PID_32:14:192:36:177"] = {rtwname: "<S8>/RAMDelayBalance"};
	this.rtwnameHashMap["<S8>/Saturation"] = {sid: "pll_PID_32:14:192:36:66"};
	this.sidHashMap["pll_PID_32:14:192:36:66"] = {rtwname: "<S8>/Saturation"};
	this.rtwnameHashMap["<S8>/SignCorrected"] = {sid: "pll_PID_32:14:192:36:55"};
	this.sidHashMap["pll_PID_32:14:192:36:55"] = {rtwname: "<S8>/SignCorrected"};
	this.rtwnameHashMap["<S8>/Switch"] = {sid: "pll_PID_32:14:192:36:79"};
	this.sidHashMap["pll_PID_32:14:192:36:79"] = {rtwname: "<S8>/Switch"};
	this.rtwnameHashMap["<S8>/insig"] = {sid: "pll_PID_32:14:192:36:46"};
	this.sidHashMap["pll_PID_32:14:192:36:46"] = {rtwname: "<S8>/insig"};
	this.rtwnameHashMap["<S8>/p50mA"] = {sid: "pll_PID_32:14:192:36:57"};
	this.sidHashMap["pll_PID_32:14:192:36:57"] = {rtwname: "<S8>/p50mA"};
	this.rtwnameHashMap["<S8>/pow2switch"] = {sid: "pll_PID_32:14:192:36:80"};
	this.sidHashMap["pll_PID_32:14:192:36:80"] = {rtwname: "<S8>/pow2switch"};
	this.rtwnameHashMap["<S8>/x4"] = {sid: "pll_PID_32:14:192:36:68"};
	this.sidHashMap["pll_PID_32:14:192:36:68"] = {rtwname: "<S8>/x4"};
	this.rtwnameHashMap["<S8>/y"] = {sid: "pll_PID_32:14:192:36:61"};
	this.sidHashMap["pll_PID_32:14:192:36:61"] = {rtwname: "<S8>/y"};
	this.rtwnameHashMap["<S9>/u"] = {sid: "pll_PID_32:14:192:35:10:480"};
	this.sidHashMap["pll_PID_32:14:192:35:10:480"] = {rtwname: "<S9>/u"};
	this.rtwnameHashMap["<S9>/bit_shift"] = {sid: "pll_PID_32:14:192:35:10:481"};
	this.sidHashMap["pll_PID_32:14:192:35:10:481"] = {rtwname: "<S9>/bit_shift"};
	this.rtwnameHashMap["<S9>/y"] = {sid: "pll_PID_32:14:192:35:10:482"};
	this.sidHashMap["pll_PID_32:14:192:35:10:482"] = {rtwname: "<S9>/y"};
	this.rtwnameHashMap["<S10>:1"] = {sid: "pll_PID_32:14:192:35:10:481:1"};
	this.sidHashMap["pll_PID_32:14:192:35:10:481:1"] = {rtwname: "<S10>:1"};
	this.rtwnameHashMap["<S11>/u"] = {sid: "pll_PID_32:14:192:36:68:480"};
	this.sidHashMap["pll_PID_32:14:192:36:68:480"] = {rtwname: "<S11>/u"};
	this.rtwnameHashMap["<S11>/bit_shift"] = {sid: "pll_PID_32:14:192:36:68:481"};
	this.sidHashMap["pll_PID_32:14:192:36:68:481"] = {rtwname: "<S11>/bit_shift"};
	this.rtwnameHashMap["<S11>/y"] = {sid: "pll_PID_32:14:192:36:68:482"};
	this.sidHashMap["pll_PID_32:14:192:36:68:482"] = {rtwname: "<S11>/y"};
	this.rtwnameHashMap["<S12>:1"] = {sid: "pll_PID_32:14:192:36:68:481:1"};
	this.sidHashMap["pll_PID_32:14:192:36:68:481:1"] = {rtwname: "<S12>:1"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
