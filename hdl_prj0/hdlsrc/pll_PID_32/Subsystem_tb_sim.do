onbreak resume
onerror resume
vsim -voptargs=+acc work.Subsystem_tb

add wave sim:/Subsystem_tb/u_Subsystem/clk
add wave sim:/Subsystem_tb/u_Subsystem/reset
add wave sim:/Subsystem_tb/u_Subsystem/clk_enable
add wave sim:/Subsystem_tb/u_Subsystem/In1_re
add wave sim:/Subsystem_tb/u_Subsystem/In1_im
add wave sim:/Subsystem_tb/u_Subsystem/ce_out
add wave sim:/Subsystem_tb/u_Subsystem/Out1
add wave sim:/Subsystem_tb/Out1_ref
add wave sim:/Subsystem_tb/u_Subsystem/Out2_re
add wave sim:/Subsystem_tb/Out2_re_ref
add wave sim:/Subsystem_tb/u_Subsystem/Out2_im
add wave sim:/Subsystem_tb/Out2_im_ref
run -all
